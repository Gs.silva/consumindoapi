﻿using ConsumindoAPI.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoAPI
{
    class Program
    {
        public static object ConsmindoAPIUsers { get; private set; }

        static void Main(string[] args)
        {
            string url = "https://randomuser.me/api";

            RandomUser randomUser = BuscarUser(url);
            Console.WriteLine("RandomUser");
            Console.WriteLine(
                String.Format("Gender: {0} \n Nome: {1} \n Location: {2} \n Email: {3} \n ")
               );


            foreach (APIUsers aPIUsers in randomUser.APIUsers)
            {
                Console.WriteLine(aPIUsers.Name.First);
            }

            
            Console.ReadLine();
        }

        public static RandomUser BuscarUser  (string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            RandomUser randomUser = JsonConvert.DeserializeObject<RandomUser>(content);
            return randomUser;
        }
    }

}

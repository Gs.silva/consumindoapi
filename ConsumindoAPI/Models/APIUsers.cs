﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsumindoAPI.Models
{
    class RandomUser
    {

        public List<APIUsers> APIUsers  { get; set; }
    }



    class APIUsers
    {
        public string Gender { get; set; }
        public Name Name{ get; set; }
        public Location Location { get; set; }
        public string Email { get; set; }
   

    }
    class Name
    {
        public string title { get; set; }
        public string First { get; set; }
        public string Last { get; set; }

    }
    class Street
    {
        public int Number { get; set; }
        public string Name { get; set; }

    }
    class Coordinates
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
    class Timeione
    {
        public string Offset { get; set; }
        public string Description { get; set; }

    }
    class Location
    {
        public Street Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public Coordinates Coordinates { get; set; }
        public Timeione Timeione { get; set; }
    }
 
}
